import type { TextEditor, Point } from 'atom'

declare module 'linter' {

  declare type Severity = 'error' | 'warning' | 'info'

  declare type Message = {|
    key: string,
    version: 2,
    linterName: string,

    // From providers
    location: {
      file: string,
      position: Range,
    },
    reference?: {
      file: string,
      position?: Point,
    },
    url?: string,
    icon?: string,
    excerpt: string,
    severity: Severity,
    solutions?: Array<{
      title?: string,
      position: Range,
      priority?: number,
      currentText?: string,
      replaceWith: string,
    } | {
      title?: string,
      priority?: number,
      position: Range,
      apply: (() => any),
    }>,
    description?: string | (() => Promise<string> | string)
  |}

  declare type MessageLegacy = {|
    key: string,
    version: 1,
    linterName: string,

    // From providers
    type: string,
    text?: string,
    html?: string,
    filePath?: string,
    range?: Range,
    class?: string,
    severity: Severity,
    trace?: Array<MessageLegacy>,
    fix?: {
      range: Range,
      newText: string,
      oldText?: string
    }
  |}

  declare type LinterResult = Array<Message | MessageLegacy> | null

  declare type Linter = {
    __$sb_linter_version: number,
    __$sb_linter_activated: boolean,
    __$sb_linter_request_latest: number,
    __$sb_linter_request_last_received: number,

    // From providers
    name: string,
    scope: 'file' | 'project',
    lintOnFly?: boolean,
    lintsOnChange?: boolean,
    grammarScopes: Array<string>,
    lint(textEditor: TextEditor): LinterResult | Promise<LinterResult>,
  }

  declare type Indie = {
    name: string,
  }

  declare type MessagesPatch = {
    added: Array<Message | MessageLegacy>,
    removed: Array<Message | MessageLegacy>,
    messages: Array<Message | MessageLegacy>,
  }

  declare type UI = {|
    name: string,
    didBeginLinting(linter: Linter, filePath: ?string): void,
    didFinishLinting(linter: Linter, filePath: ?string): void,
    render(patch: MessagesPatch): void,
    dispose(): void
  |}

}
