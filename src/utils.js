'use babel'

export function getCurrentEditor () {
  return atom.workspace.getActiveTextEditor()
}


export function filterMessagesBySeverity (messages: Set<linter.Message>, severity: linter.Severity): Array<linter.Message> {

  const filterBySeverity = message =>
    message.severity === severity

  return Array
    .from(messages)
    .filter(filterBySeverity)
}


export function filterMessagesByPath (messages: Set<linter.Message>, path: string): Array<linter.Message> {
  if (!path || !messages)
    return null

  const filterByPath = message =>
    getPathForMessage(message) === path

  return Array
    .from(messages)
    .filter(filterByPath)
}


export function getPathForMessage (message) {
  if (!message)
    return null
  if (message.location)
    return message.location.file
  if (message.filePath)
    return message.filePath
  return null
}

// export function filterMessagesByEditor (editor, ...messages) {
//   const messageInEditor = message  => getPathForMessage(message) === path
//   let path     = editor ? editor.getPath() : null
//   return messages.filter(messageInEditor)
// }
//
// export function editorIsVisible (editor) {
//   if (editor.isDestroyed())
//     return false
//   let { display } = getComputedStyle(editor.getElement())
//   return display !== 'none'
// }
//

// function provideOverlayToActiveEditor (editor) {
//
// }


// const linterURI = uri => 0 === 'linter://'.indexOf(uri)


// function registerOpener (viewClass) {
//   let op = function(uri) {
//     if (linterURI(uri))
//       return new viewClass(uri)
//   }
//
//   return atom.workspace.addOpener(op)
// }
//
//
// /**
//  * Registers a new view provider to the global view registry. Also assigns
//  *
//  * @method registerViewProvider
//  *
//  * @param  {constructor}             model A class for the model that a view is registered for
//  * @param  {constructor}             view  Bound view's constructor
//  *
//  * @return {Disposable}             A disposable for the registered view provder
//  */
//
//  function registerViewProvider (model, view) {
//
//   if (!(view.item &&
//         view.getItem ||
//        (view.prototype && view.prototype.getItem)))
//     throw new Error("The view " + view.name + " should implement a getItem method")
//
//   model.prototype.getElement = function () {
//     if (this.element)
//       return this.element
//   }
//
//   const provideView = (/*obj*/) => {
//     return new view()
//     // let v          = new view()
//     // v.model        = obj
//     // obj.view       = v
//     // return typeof v.getItem === 'function' ? v.getItem() : v.item
//   }
//
//   return atom.views.addViewProvider(model, provideView)
// }
