'use babel'

// @flow
import { Emitter } from 'event-kit'
import type { Disposable } from 'event-kit'

import { filterMessagesBySeverity } from '../utils'

const _messages = Symbol('linting-errors')
const _emitter  = Symbol('event-emitter')


export default class LinterEventsManager {

  name = 'Errors Overview'

  constructor () {
    this[_emitter]  = new Emitter()
    this[_messages] = new Set()
    this.didBeginLinting = () => null  // eslint-disable-line block-padding/functions
    this.didFinishLinting = () => null // eslint-disable-line block-padding/functions
  }

  get messages (): Set<linter.Message> {
    return this[_messages]
  }

  set messages (messages = []) {
    this[_messages] = new Set(messages)
    this[_emitter].emit('did-update-messages', this[_messages])
    return this[_messages]
  }

  get info (): Array<linter.Message> {
    return filterMessagesBySeverity(this.messages, 'info')
  }

  get warnings (): Array<linter.Message> {
    return filterMessagesBySeverity(this.messages, 'warning')
  }

  get errors (): Array<linter.Message> {
    return filterMessagesBySeverity(this.messages, 'error')
  }

  get infoCount (): number {
    return this.info.length
  }

  get warningsCount (): number {
    return this.warnings.length
  }

  get errorsCount (): number {
    return this.errors.length
  }

  render (payload) {
    this.messages = payload.messages
  }

  dispose () {
    this[_emitter].dispose()
  }

  destroy () {
    this.dispose()
  }


  onDidUpdate (fn: Function): Disposable {
    return this[_emitter].on('did-update-messages', fn)
  }

}
