'use babel'

// @flow
import LinterEventsManager from './models/LinterEventsManager'
import createOverviewElement from './views/OverviewTile'


let manager = null
let statusBarTile = null


export const config = require('./configuration.json')


export function activate () {
  manager = new LinterEventsManager()
}

export function deactivate () {
  if (manager) manager.destroy()
  if (statusBarTile) statusBarTile.destroy()
}

export function provideLinterUI () {
  return manager
}

export function consumeStatusBar (bar) {
  const item = createOverviewElement(manager)
  statusBarTile = bar.addLeftTile({ item })
  console.warn("Status bar ~", statusBarTile)
  window.overviewTile = item
  return statusBarTile
}
