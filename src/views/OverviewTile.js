'use babel'

// @flow
import self from 'autobind-decorator'
import React, { Fragment, PureComponent } from 'react'
import { render } from 'react-dom'
import { CompositeDisposable } from 'event-kit'

// FIXME
type Manager = Object

type Props = {
  manager: Manager,
}

type State = {
  messages: Set<linter.Message>,
  active: linter.Severity | null,
  warnings: number,
  errors: number,
  info: number,
}


class Overview extends PureComponent<Props, State> {

  subscriptions: CompositeDisposable

  state = {
    messages: new Set(),
    active:   null,
    warnings: 0,
    errors:   0,
    info:     0,
  }

  componentDidMount () {
    this.subscriptions = new CompositeDisposable()
    const manager      = this.props.manager

    this.subscriptions.add(
      manager.onDidUpdate(this.updateMessages)
    )
  }

  componentWillUnmount () {
    this.subscriptions.dispose()
  }

  @self
  updateMessages (messages: Set<linter.Message>) {
    this.setState({
      messages,
      warnings: this.props.manager.warningsCount,
      errors:   this.props.manager.errorsCount,
      info:     this.props.manager.infoCount,
    })
  }

  getMessagesForOverlay () {
    return this.props.manager[this.state.active] || []
  }

  showOverlay (active) {
    if (this.state.active === active)
      active = null
    this.setState({ active })
  }

  render () {


    const Section = props => {

      const showOverlay = () =>
        this.showOverlay(props.severity)

      const className = props.severity.endsWith('s') ? props.severity.substr(0, props.severity.length - 1) : props.severity
      return <span
        onClick={ showOverlay }
        className={ `entry status-${className}` }>
        <strong className='severity'>{ props.severity }</strong>
        <output className='value'>{ this.state[ props.severity ]}</output>
      </span>
    }

    return <Fragment>
      <Section severity='info' />
      <Section severity='warnings' />
      <Section severity='errors' />
      { this.renderOverlay() }
    </Fragment>
  }

  renderOverlay () {
    if (this.state.active === null)
      return null

    return <div className='list-overlay'>
      MODAALI ÄKS DEE { this.state.active }
      <ul>

        { this.getMessagesForOverlay().map(msg =>
          // FIXME: Linter message version 1
          <li>{ msg.excerpt }</li>)
        }

      </ul>
    </div>
  }
}


export default function createOverviewElement (manager: Manager) {

  const el = document.createElement('section')
  el.classList.add('linter-overview')

  const assignReference = ref =>
    el.component = ref

  render(
    <Overview
      ref={ assignReference }
      manager={ manager }
    />,
    el)

  return el
}
